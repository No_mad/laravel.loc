<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\SubcommentController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [CommentController::class, 'getAllComments'])
    ->name('home')
    ->middleware('auth');
Route::get('getUserComments', [CommentController::class, 'getUserComments'])
    ->name('showUserComments');
Route::get('publish', [CommentController::class, 'viewPublish'])
    ->name('publishPage');
Route::post('publish', [CommentController::class, 'publish']);
Route::get('edit/{id}', [CommentController::class, 'viewEdit'])
    ->name('editPage');
Route::post('edit', [CommentController::class, 'edit']);
Route::get('publishSubcomment/{id}', [SubcommentController::class, 'viewPublishSubcomment'])
    ->name('publishSubPage');
Route::post('publishSubcomment', [SubcommentController::class, 'publishSubcomment']);
Route::get('checkEmail/{email}', [\App\Http\Controllers\UserController::class, 'checkEmail'])
    ->name('checkEmailMethod');


