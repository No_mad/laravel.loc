<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Subcomment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubcommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Subcomment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'text_subcomments' => $this->faker->text(),
            'id_users' => User::all()->random()->id,
            'id_comments' => Comment::all()->random()->id_comments
        ];
    }
}
