<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcomments', function (Blueprint $table) {
            $table->id('id_subcomments');
            $table->string('text_subcomments', 255);
            $table->unsignedBigInteger('id_users');
            $table->unsignedBigInteger('id_comments');

            $table->foreign('id_users')->references('id')->on('users');
            $table->foreign('id_comments')->references('id_comments')->on('comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subcomments', function (Blueprint $table) {
            $table->dropForeign(['id_users']);
            $table->dropForeign(['id_comments']);
        });
        Schema::dropIfExists('subcomments');
    }
}
