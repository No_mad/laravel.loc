$(function () {
    let validEmail = false;
    let validPass = false;

    $("form").submit(function (event) {
        event.preventDefault();
        let emailInput = $("#email");
        let passInput = $("#password");

        let email = emailInput.val();
        let pass = passInput.val();

        $("div.appended").empty();

        if (email == "") {
            emailInput.addClass("is-invalid");
            $("div.emailDiv").append("<div class='invalid-feedback appended'> Email is required </div>");
        } else {
            emailInput.removeClass("is-invalid");
            validEmail = true;
        }

        if (pass == "") {
            passInput.addClass("is-invalid");
            $("div.passDiv").append("<div class='invalid-feedback appended'> Password is required </div>");
        } else {
            passInput.removeClass("is-invalid");
            validPass = true;
        }

        if (validEmail == true && validPass == true) {
            $("form").unbind('submit').submit();
        }
    });
});
