$(function () {
    $("#email").blur(function () {
        $("div.appended").empty();
        let emailVal = $("#email").val();
        $("#register_button").attr('disabled', 'disabled');
        if(emailVal){
        $.ajax({
            url: "/checkEmail/" + emailVal,
            method: "GET",
            success: function (data) {
                let ret = data.msg;
                if (ret == true) {
                    $("#register_button").attr('disabled', false);
                    $("#errorsHere").append("<div class='alert-success appended'>Email is available</div>");
                } else {
                    $("#errorsHere").append("<div class='alert-danger appended'>Email already exists</div>");
                }
            },
            error: function (jqXHR) {
                alert("ajax error: " + jqXHR.status);
            }
        });
        }else{
            $("#errorsHere").append("<div class='alert-danger appended'>Email field is empty</div>");
        }
    });
});
