<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcomment extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_subcomments';
    public $timestamps = false;
    protected $fillable = ['text_subcomments','id_users','id_comments'];

    public function user()
    {
        return $this->belongsTo(User::class,'id_users','id');
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class,'id_comments');
    }
}
