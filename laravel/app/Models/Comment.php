<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_comments';
    public $timestamps = false;
    protected $fillable = ['text_comments','id_users'];

    public function user()
    {
        return $this->belongsTo(User::class,'id_users','id');
    }

    public function subcomments()
    {
        return $this->hasMany(Subcomment::class, 'id_comments');
    }
}
