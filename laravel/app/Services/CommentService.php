<?php

namespace App\Services;


use App\Repositories\CommentRepository;

class CommentService extends BaseService
{
    public function __construct(CommentRepository $repo)
    {
        $this->repo = $repo;
    }

    public function findByUserIdPagination(int $id, int $perPage)
    {
        return $this->repo->findByUserIdPagination($id,$perPage);
    }

    public function allPagination(int $perPage){
        return $this->repo->allPagination($perPage);
    }
}
