<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\SubcommentRepository;

class SubcommentService extends BaseService
{
    public function __construct(SubcommentRepository $repo)
    {
        $this->repo = $repo;
    }
}
