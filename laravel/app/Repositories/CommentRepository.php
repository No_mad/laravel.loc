<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Comment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CommentRepository extends BaseRepository
{
    public function __construct(Comment $model)
    {
        $this->model = $model;
    }

    public function update(int $id, array $data)
    {
        $query = $this->model->where('id_comments', $id)->first();
        return $query->update($data);
    }

    public function findByUserIdPagination(int $id,int $perPage)
    {
        return $this->model->where('id_users', $id)->paginate($perPage);
    }

    public function allPagination(int $perPage): LengthAwarePaginator
    {
        return $this->model::paginate($perPage);
    }
}
