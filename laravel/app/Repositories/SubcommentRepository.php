<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Subcomment;
use Illuminate\Database\Eloquent\Model;

class SubcommentRepository extends BaseRepository
{
    public function __construct(Subcomment $model)
    {
        $this->model=$model;
    }
}
