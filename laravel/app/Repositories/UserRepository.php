<?php

namespace App\Repositories;


use App\Models\User;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function isRecordExists(string $column, string $value)
    {
        if ($this->model->where($column, $value)->exists()) {
            return true;
        } else {
            return false;
        }
    }
}
