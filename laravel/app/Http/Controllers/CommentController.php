<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\CommentService;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    public function getAllComments()
    {
        $comments = $this->commentService->allPagination(5);

        return view('home', ['data' => $comments]);
    }

    public function getUserComments()
    {
        $comments = $this->commentService->findByUserIdPagination(Auth::id(),5);

        return view('showUserComments', ['data' => $comments]);
    }

    public function viewPublish()
    {
        return view('publish');
    }

    public function publish(Request $request)
    {
        $request->validate([
            'text' => 'required'
        ]);

        $this->commentService->create([
            'text_comments' => $request->text,
            'id_users' => Auth::id()
        ]);
        return redirect()->route('home');
    }

    public function viewEdit($id)
    {
        return view('edit', ["id" => $id]);
    }

    public function edit(Request $request)
    {
        $request->validate([
            'text' => 'required'
        ]);

        $comment = $this->commentService->findById($request->id);
        if ($comment->id_users == Auth::id()) {
            $this->commentService->update($request->id, ['text_comments' => $request->text]);
        }
        return redirect()->route('home');
    }

}
