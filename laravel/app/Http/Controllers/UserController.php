<?php

namespace App\Http\Controllers;


use App\Services\UserService;


class UserController extends Controller
{
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function checkEmail(string $email)
    {
        if (!empty($email)) {
            if ($this->userService->isRecordExists('email', $email)) {
                return response()->json(['msg' => false]);
            } else {
                return response()->json(['msg' => true]);
            }
        } else {
            return response()->json(['msg' => false]);
        }
    }
}
