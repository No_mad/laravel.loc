<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\SubcommentService;
use App\Services\CommentService;


class SubcommentController extends Controller
{
    public function __construct(SubcommentService $subcommentService, CommentService $commentService)
    {
        $this->subcommentService = $subcommentService;
        $this->commentService = $commentService;
    }

    public function viewPublishSubcomment($id)
    {
        return view('publishSub', ["id" => $id]);
    }

    public function publishSubcomment(Request $request)
    {
        $request->validate([
            'text' => 'required'
        ]);

        $comment = $this->commentService->findById($request->id);
        if ($comment->id_users != Auth::id()) {
            $this->subcommentService->create([
                'text_subcomments' => $request->text,
                'id_users' => Auth::id(),
                'id_comments' => $comment->id_comments
            ]);
        }
        return redirect()->route('home');
    }

}
