@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row justify-content-between">
                    <div class="col-auto me-auto">
                        <a class="btn btn-primary" href="{{route('publishPage')}}">Create new comment</a>
                    </div>
                    <div class="col-auto">
                        <a class="btn btn-primary" href="{{route('home')}}" >Show all comments</a>
                    </div>
                </div>
                <br/>
                @foreach($data as $comment)
                    <div class="card">
                        <div class="card-header">
                            <p>{{__('Owner:')}}</p>
                            {{$comment->user['name']}}
                        </div>
                        <div class="card-body">
                            <p>{{__('Text:')}}</p>
                            {{$comment->text_comments}}
                            <p></p>
                            <div class="col-sm-12">
                                @foreach($comment->subcomments as $subcomment)
                                    <div class="card">
                                        <div class="card-header">
                                            <p>{{__('Owner:')}}</p>
                                            {{$subcomment->user['name']}}
                                        </div>
                                        <div class="card-body">
                                            <p>{{__('Text:')}}</p>
                                            {{$subcomment->text_subcomments}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row-cols-2">
                                @if(Auth::id()==$comment->id_users)
                                    <a class="btn btn-primary" href="{{route('editPage',$comment->id_comments)}}"
                                       role="button">
                                        Edit comment
                                    </a>
                                @else
                                    <a class="btn btn-primary" href="{{route('publishSubPage',$comment->id_comments)}}"
                                       role="button">
                                        Create subcomment
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach

                {{ $data->links() }}
            </div>
        </div>
    </div>
@endsection

