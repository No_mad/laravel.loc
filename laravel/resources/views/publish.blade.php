@extends('layouts.app')

@section('content')
    <form action="/publish" method="POST">
        @csrf
        <div class="container">
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Enter text of your new comment</label>
                <textarea class="@error('text') is-invalid @enderror form-control" id="exampleFormControlTextarea1" name="text" rows="3"></textarea>
                @error('text')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="row-cols-2">
                <button type="submit" class="btn btn-primary">Create new comment</button>
                <a class="btn btn-primary" href="{{route('home')}}" role="button">Back to home page</a>
            </div>
        </div>
    </form>
@endsection
