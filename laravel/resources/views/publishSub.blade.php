@extends('layouts.app')

@section('content')
    <form action="/publishSubcomment" method="POST">
        @csrf
        <div class="container">
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Enter text of new Subcomment</label>
                <textarea class="@error('text') is-invalid @enderror form-control" id="exampleFormControlTextarea1" name="text" rows="3"></textarea>
                @error('text')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <input type="hidden" name="id" value="{{$id}}">
            <div class="row-cols-2">
                <button type="submit" class="btn btn-primary">Publish Subcomment</button>
                <a class="btn btn-primary" href="{{route('home')}}" role="button">Back to home page</a>
            </div>
        </div>
    </form>
@endsection

