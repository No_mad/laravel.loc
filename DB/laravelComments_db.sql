-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Сен 13 2021 г., 13:08
-- Версия сервера: 5.7.35-0ubuntu0.18.04.1
-- Версия PHP: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `laravelComments_db`
--
CREATE DATABASE IF NOT EXISTS `laravelComments_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `laravelComments_db`;

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id_comments` bigint(20) UNSIGNED NOT NULL,
  `text_comments` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_users` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id_comments`, `text_comments`, `id_users`) VALUES
(11, 'I edited this comment!', 1),
(12, 'Fugit eum porro cum ut consequatur autem est. Sit rerum deserunt et veniam non suscipit ut. Voluptatem illo quisquam animi facilis qui qui. Similique labore qui incidunt excepturi pariatur.', 41),
(13, 'Doloremque dolorem iure dignissimos a sit corporis odio. Et voluptatem porro nihil. Quia unde unde eius est. A odit magnam rem hic.', 38),
(14, 'Veniam itaque id et. Asperiores sed odio numquam optio. Aut voluptas dolore et et in. Repellendus autem totam ex et iste ipsam. Consequatur quo dignissimos suscipit distinctio porro omnis magnam hic.', 36),
(15, 'Distinctio asperiores distinctio sunt harum. Laborum quasi ea dolor assumenda maiores. Dolorem voluptas et dolorem et esse aperiam. Quo maxime sint distinctio voluptatem.', 33),
(16, 'Non beatae veritatis sint nihil exercitationem. Alias error voluptatem eum sint cumque voluptas. Odio iste iusto nobis sit alias distinctio. Animi tenetur quam natus unde numquam.', 39),
(17, 'Autem tempore excepturi sit corporis nihil. A qui aut nesciunt ut neque. Autem ex rerum ut.', 34),
(18, 'Fugit et quia similique molestiae fuga et est. Perferendis velit expedita fuga provident veniam dolores. Nam libero quae hic et temporibus iure nostrum.', 32),
(19, 'Eius impedit eligendi repellat beatae. Saepe praesentium quia qui. Quisquam voluptas totam minima dolor.', 34),
(20, 'Nostrum labore voluptatem laudantium et et. Consequuntur dolores ipsam inventore voluptatibus est. Incidunt asperiores aut quia voluptatem nam.', 39),
(21, 'My first comment!', 1),
(22, 'Edited test comment', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2021_09_11_122340_create_comments__table', 2),
(9, '2021_09_11_155511_create_subcomments__table', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `subcomments`
--

CREATE TABLE `subcomments` (
  `id_subcomments` bigint(20) UNSIGNED NOT NULL,
  `text_subcomments` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_users` bigint(20) UNSIGNED NOT NULL,
  `id_comments` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `subcomments`
--

INSERT INTO `subcomments` (`id_subcomments`, `text_subcomments`, `id_users`, `id_comments`) VALUES
(1, 'Dolor consequatur iure sed repudiandae expedita. Laborum consectetur hic natus quis. Ut soluta consequatur harum facere quia. Molestiae et quasi itaque eum quae.', 32, 12),
(2, 'Voluptas eaque illum temporibus sint. Sequi cupiditate fuga minima. Saepe ut quasi eum et. Nobis quaerat qui exercitationem recusandae rem.', 40, 15),
(3, 'Aperiam sit ullam ullam. Aliquid illum aut aut aut placeat quis culpa excepturi. Expedita et temporibus asperiores quo amet. Qui reiciendis nobis veritatis qui. Totam est ut debitis et occaecati.', 39, 14),
(4, 'Ipsa in ea laboriosam rem quis est. Laudantium et vitae dolores delectus et id aut. Aut reiciendis id atque ea natus et. Ea recusandae eum nisi nobis. Dolore maiores et sint id et ea.', 38, 16),
(5, 'Perspiciatis repudiandae sed eos impedit debitis qui reiciendis. Repudiandae sit aut voluptatum et quo dolore aspernatur. Sunt cupiditate possimus odit vel. Et fugiat occaecati eveniet aut ullam.', 39, 14),
(6, 'Impedit repellat quam quibusdam qui quam. Quisquam aliquid molestiae qui quod rerum. Quaerat quis excepturi quaerat omnis itaque dolorem repudiandae accusamus. Itaque molestiae occaecati eius.', 32, 14),
(7, 'Perferendis assumenda voluptas laudantium. Quis et animi quod ea. Quidem laborum qui voluptatem molestiae qui blanditiis dolores.', 32, 15),
(8, 'Sint aliquam tenetur et nihil laudantium tenetur. Quis perferendis qui exercitationem possimus. Ipsam aut fugiat est dolorum nobis autem consectetur.', 35, 15),
(9, 'Qui iste ut repellendus at vel corrupti. Dignissimos omnis consectetur nesciunt ut suscipit culpa. Necessitatibus quidem aliquam sed quae et. Iste harum tenetur id.', 1, 19),
(10, 'Quibusdam sapiente beatae maiores similique perspiciatis. Commodi quia accusamus expedita ut nihil autem ad. Qui autem sed id. Dolores quo qui quis suscipit eos deleniti sit excepturi.', 37, 20),
(11, 'What kind of a language is that? Is it alien?', 1, 12),
(12, 'bruh', 1, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jhon', 'jhon@test.com', NULL, '$2y$10$B4/JHpA6C4G1nidW/QXbjO5u0Ejx/dYAUnZDZdtq7ur6p7lHPzy0C', NULL, '2021-09-11 12:04:08', '2021-09-11 12:04:08'),
(32, 'Dr. Merlin Cronin', 'jazmin.mueller@example.net', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0VTzUVl6kU', '2021-09-11 17:27:20', '2021-09-11 17:27:20'),
(33, 'Prof. Lula Auer DDS', 'schaden.olin@example.net', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KqxDZJ3nZt', '2021-09-11 17:27:20', '2021-09-11 17:27:20'),
(34, 'Major Hansen', 'rickie.hahn@example.com', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'E382e5iRLH', '2021-09-11 17:27:20', '2021-09-11 17:27:20'),
(35, 'Ervin Fahey', 'huel.lincoln@example.com', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'AKYPJBrG6Q', '2021-09-11 17:27:20', '2021-09-11 17:27:20'),
(36, 'Uriel Schmidt Jr.', 'beatty.lucius@example.net', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'lgQmfRnOWT', '2021-09-11 17:27:20', '2021-09-11 17:27:20'),
(37, 'Jillian Hackett', 'carole61@example.net', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'hsLF4iAEtH', '2021-09-11 17:27:20', '2021-09-11 17:27:20'),
(38, 'Elenor Schiller', 'ashanahan@example.org', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'WkY6ZWVwTn', '2021-09-11 17:27:20', '2021-09-11 17:27:20'),
(39, 'Russel Pagac', 'mario.farrell@example.net', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0awSwyFEzF', '2021-09-11 17:27:20', '2021-09-11 17:27:20'),
(40, 'Alyce Streich DDS', 'ispinka@example.com', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iur1IAIK5n', '2021-09-11 17:27:20', '2021-09-11 17:27:20'),
(41, 'Viva Lesch', 'agnes.fahey@example.org', '2021-09-11 17:27:20', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6eStSuszB0', '2021-09-11 17:27:20', '2021-09-11 17:27:20');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id_comments`),
  ADD KEY `comments_id_users_foreign` (`id_users`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Индексы таблицы `subcomments`
--
ALTER TABLE `subcomments`
  ADD PRIMARY KEY (`id_subcomments`),
  ADD KEY `subcomments_id_users_foreign` (`id_users`),
  ADD KEY `subcomments_id_comments_foreign` (`id_comments`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id_comments` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `subcomments`
--
ALTER TABLE `subcomments`
  MODIFY `id_subcomments` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `subcomments`
--
ALTER TABLE `subcomments`
  ADD CONSTRAINT `subcomments_id_comments_foreign` FOREIGN KEY (`id_comments`) REFERENCES `comments` (`id_comments`),
  ADD CONSTRAINT `subcomments_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
